# Water_Stress2018

#★retrain2.pyの実行方法（学習および検証）

retrain2.pyの117行目のsimizuを自分のユーザー名にする。

自分のホーム上にtmpというデュレクトリを作成する 

cd /home/simizu 

mkdir tmp

モジュールをキャッシュする URL からモジュールを作成するとき、モジュールのコンテンツは
ローカルシステムの一時ディレクトリにダウンロードされてキャシュされます。 
モジュールがキャッシュされる位置は TFHUB_CACHE_DIR 環境変数を使用して override されます。
tmpを作成したら以下を実行する。

export TFHUB_CACHE_DIR=~/tmp

＜学習実行例＞

python3 retrain2.py \

--how_many_training_steps=5000 \     

学習回数の指定

--summaries_dir=training_summaries/basic \

Tensorboardで学習過程を可視化する時に使う（デュレクトリの名前は任意）

--bottleneck_dir=bottlenecks \　　

特徴量を保存する（デュレクトリの名前は任意）

--saved_model_dir=inception_resnet_v2 \　

学習済みモデルを保存する（デュレクトリの名前は任意）

--output_graph=retrained_graph.pb \ 　

image_test.pyを使用する際に使う.pbファイル（ファイル名は任意）

--output_labels=retrained_labels.txt \   

image_test.pyを使用する際に使う.txtファイル（ファイル名は任意）

--tfhub_module=https://tfhub.dev/google/imagenet/inception_resnet_v2/feature_vector/1 \

使用する学習済みモデルの指定（この例ではInception_resnet_v2を使用している）

--train_dir=train_class2_18h_sortout_segmentation9_DA/ 

教師用データの指定

--validation_dir=validation_class2_18h_sortout_segmentation9_DA/ \

検証用データの指定

--test_dir=test_class2_18h_sortout_segmentation9_DA/ \

テスト用データの指定

--print_misclassified_test_images

テストで誤判別された画像を表示

※それぞれの保存デュレクトリ名およびファイル名は任意（実行ごとにファイル名は変更する）

#image_test.pyの実行方法（学習器利用方法）

python3 image_test.py \

--image=判別したい画像のデュレクトリを指定 \

--graph=retrained_graph.pbが保存されているデュレクトリを指定 \

--labels=retrained_labels.txtが保存されているデュレクトリを指定 \


#Tensorboardの利用方法（学習および検証の可視化）

--summaries_dir=training_summaries/basicとした場合

training_summariesが保存されているデュレクトリに行き以下のコマンドを実行する

tensorboard --logdir=training_summaries　


#rotation.pyの利用方法（画像を回転させるときに使う）

python3 rotation.py

※プログラム内で入力と出力のデュレクトリを指定する行は変えてください